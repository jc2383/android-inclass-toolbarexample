package com.example.toolbarexample;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;

public class AccountProfile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_profile);

        // setup the toolbar --> set the toolbar2 to be the default toolbar
        Toolbar tb = (Toolbar) findViewById(R.id.toolbar2);
        this.setSupportActionBar(tb);


        // this adds the <- icon to the toolbar
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);


    }
}
