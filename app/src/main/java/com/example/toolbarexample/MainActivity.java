package com.example.toolbarexample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

// 1. Import the correct Toolbar package
import androidx.appcompat.widget.Toolbar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 2. Create your toolbar variable
        Toolbar t1 = (Toolbar) findViewById(R.id.toolbar);
        // 3. Tells Android that t1 is your PRIMARY toolbar at the top of the screen
        setSupportActionBar(t1);
    }



    // Tell the toolbar about your menu options
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // tells android to load the res/menu/menu_main.xml
        // file as your tool bar menu items
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    // Create click handlers for your menu options
    // Control what happens when you click on each menu itme
    // item parameter contains the menu item the person clicked on
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // 1. Figure out what menu option the person clicked on
        int selectedMenuItem = item.getItemId();

        switch(selectedMenuItem) {
            case R.id.miAccountProfile:
                Intent i = new Intent(MainActivity.this, AccountProfile.class);
                i.putExtra("username", "peter");
                startActivity(i);
                return true;
            case R.id.miContact:
                Toast t1 = Toast.makeText(getApplicationContext(), "You clicked on Contact", Toast.LENGTH_SHORT);
                t1.show();
                return true;
            case R.id.miHelp:
                Toast t2 = Toast.makeText(getApplicationContext(), "You clicked on help", Toast.LENGTH_SHORT);
                t2.show();
                return true;
            default:
                // do nothing
        }

        // default android code
        return super.onOptionsItemSelected(item);
    }



}
